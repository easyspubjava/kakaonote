# kakaonote

## design pattern & refactoring 

0. 이 강좌에서 알수 있는 이야기들...

1. 생성을 위한 패턴들과 그 응용 
   - Singleton, Factory, Abstract Factory, Builder

   - Factory Method를 활용한 refactoring

2. 조건에 따른 여러가지 상태와 전략을 클래스로 표현하는 패턴

   - Strategy Pattern, State Pattern, Obsrver, Memento

   - if - else if 를 클래스로 만들어 refactoring 
 
   - 상태에 따른 클래스화 refactoring


3. 동일시하기와 위임하기 

   - Decorator, Composite, Proxy, Adapter

   - 상속을 위임으로 만드는 refactoring

   - Coffee 만들기 

4. 하위 클래스에 위임하기 

   - Template Method

   - 추상 클래스를 활용한 프레임 워크 

5. 기능과 구현을 분리하는 

   - Bridge

6. 구조를 단순하게 

   - Facade, Mediator

7. 구조를 순회하는 패턴

   - Iterator, Visitor, Chain of responsiblity

   - visitor를 활용한 기능의 refactoring

8. 낭비를 없애기 (재활용) 

    - Flyweight


9. 자주 사용되는 리펙토링들... 

   - 상태를 클래스로 만들기

   - 널 객체 활용하기

   - 예외 처리하기
